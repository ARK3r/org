---
title: 'Week of 09/20/2021'
description: Catch up with the latest updates from Grey Software!
category: 'Weekly Updates'
position: 3004
---

## Overview

This week, we as a team didn't manage to close any issues, but we made solid progress across all our tasks!

<cta-button text="Sprint Team Board" link="https://gitlab.com/groups/grey-software/-/boards/3080709?scope=all&iteration_title=Week%20of%20Sep-20th%20-%20v4.1%20Sprint%203"></cta-button>

> Reminder of our major goals from last week
>
> 1. Update the investor pitch as feedback comes.
> 2. Follow up with UET Mardan about Final Year Project collaboration.
> 3. Promote Grey Software and Focused Browsing more actively on Hackernews, Product Hunt, Reddit, etc.

### What we did this past week

1. We made updates to our pitch deck, and we feel like we're getting closer to our ideal pitch, but we still have some
   work to do. We received awesome feedback from the Pioneer community last week and consulted great resources like The
   Futur, Underscore.vc, and Harvard iLab's Startup Secrets.

2. We emailed UET Mardan and started documenting how we will collaborate with UET Mardan during this upcoming academic
   year.

3. We posted pretty much everywhere but Product Hunt. dev.to got the best results, with only 5 visits.

4. We came up with new branding copy that we will be testing out this week! We updated our websites and social media
   pages with it.

### Our goals for Next Week

1. Publish a polished final draft of our pitch to raise non-equity-dilutive VC capital.

2. Prepare Grey Software for Hacktoberfest

## Team Updates

### Raj

This week, I continued to create
[the landing website designs in my queue](https://gitlab.com/groups/grey-software/-/issues?scope=all&state=all&milestone_title=v4.1&assignee_username[]=teccUI)
and [work with Arsala to refine our pitch to raise capital](https://gitlab.com/grey-software/org/-/issues/108).

### Faraz

This week, I implemented the updated SponsorCard designs and created a seperate section for past sponsors in credits
page.

I was also involved in
[moving our automation scripts from the organization repository to a new automation repository](). The automation
website will be published soon, and here are
[some changes we've made so far](https://gitlab.com/grey-software/automation/-/merge_requests/1):

- The aggregate-analytics script takes in a command line parameter that specifies the time period for which the user
  wants to fetch analytics.
- We re-factored the scripts to use environment variables
- We began documenting the usage of each script

### Laiba

This week I was responsible for planning
[how Grey Software will collaborate with UET Mardan during this upcoming academic year](https://gitlab.com/grey-software/resources/-/issues/12).

As an alumnus of the University and somebody who wants the best for my juniors, I am excited to help the next generation
of students have an even better experience working with Grey Software than I did.

Our current plan is to have Grey software supervise two FYP (Final Year Project) teams and run workshops throughout the
year to help students manage their projects and follow best practices.

### Zakir

This week I worked on updating the Navbar and Footer Components across the Grey Software web ecosystem!

I opened MRs on the
[`grey-docs`](https://gitlab.com/grey-software/templates/grey-docs/-/merge_requests?scope=all&state=opened&assignee_username=ZakirBangash)
and
[`website`](https://gitlab.com/grey-software/website/-/merge_requests?scope=all&state=opened&assignee_username=ZakirBangash)
repos, and hope to see my work merged in by next week.

### Saifullah

This week I worked on [developing the `/team` page](https://gitlab.com/grey-software/website/-/issues/80) on our landing
website, and I started with
[developing our profile card component](https://gitlab.com/grey-software/website/-/issues/81).

### Abdurrahman

This week I experimented with updates to our markdown article pages, and
[updated our tailwind spacing configuration with a new set of values](https://gitlab.com/grey-software/website/-/merge_requests/88)
that are designed to make it easier for designers and developers to communicate about UI.
