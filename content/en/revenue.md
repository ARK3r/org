---
title: Overview
description: An overview of Grey Software's revenue channels.
category: Revenue Channels
position: 2000
---

Sustainable revenue is, in the most literal sense, vital for our organization. Our ability to generate revenue will also
be a marker for how intelligently our organization delivers value to people.

The revenue channels we're targeting are:

- 🥅 [Revenue by offering App Collection Subscriptions](https://gitlab.com/groups/grey-software/-/epics/3)
- 🥅
  [Revenue by offering advertising space throughout our ecosystem](https://gitlab.com/groups/grey-software/-/epics/17)
- 🥅
  [Capital through Founding Partner Packages to Angel Investors and VCs](https://gitlab.com/groups/grey-software/-/epics/24)
